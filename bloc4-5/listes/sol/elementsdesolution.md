# La structure de données liste - éléments de solution

## Construire des fonctions avancées avec les primitives

Voici un fichier [extendedlistsol.py](extendedlistsol.py) pour la La classe `Extendedlist` avec toutes les fonctions implémentées (et plus encore...).

Avant de regarder la proposition de correction pour une implémentation
récursive de `reverse`, essayez de construire une solution en créant
une méthode auxiliaire pour ajouter un paramètre accumulateur à la
méthode `reverse`. Ce paramètre sert à construire le résultat du
calcul, i.e. la liste renversée au fur et à mesure des appels
récursifs.

## Itérer sur des listes plutôt qu'accéder au i-ème élément

Voici un fichier [experience.py](experience.py).

> Note: depuis la première version, le sujet a été modifié pour utiliser le module `timeit` plutôt que `time` qui peut poser des problèmes de prise de temps sur certaines machines.

Et le résultat qu'il produit :

![resultatexperience.png](resultatexperience.png)

- comparer les temps d'exécution des deux fonctions pour notre implantation, pouvait-on s'attendre au résultat observé ?
  
  Oui. L'utilisation de `get` prend plus de temps que l'itération. En
  effet la méthode `__next__` s'exécute en temps constant (un appel à
  `head` et un appel à `tail`). Alors que l'utilisation de `get`
  implique un parcours de liste à chaque appel, d'où une complexité en
  $`\Theta(n^2)`$ (1 appel à `head` pour `get[0]`, 1 appel à `head` et
  à `tail` pour `get[1]`, 1 appel à `head` et 2 appels à `tail` pour
  `get[2]`, etc.).

  On peut observer uniquement les résultats sur notre implantation de liste et comparer aux fonctions $`x`$ et $`x^2`$ pour rechercher empiriquement la complexité en temps (on repasse sur des axes linéaires).
  
  ![resultatexperience_el.png](resultatexperience_el.png)
  
- comparer les temps d'exécution des deux fonctions pour les listes natives de Python, pouvait-on s'attendre au résultat observé ?

  On retrouve le fait que le `get` est moins efficace que l'itération. Pour autant, leur complexité est linéaire dans les deux cas.
  
  ![resultatexperience_pl.png](resultatexperience_pl.png)
  
  Nous vous renvoyons au paragraphe explicatif de la fin du sujet de TP pour plus de détails.
