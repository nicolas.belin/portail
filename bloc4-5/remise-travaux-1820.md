Remise travaux blocs 4 et 5 - 2019/20
=====================================

D'ici au 31 décembre 2020, vous devez rendre vos travaux

* [Icewalker](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/tree/master/bloc4-5/icewalker)
* [Projet COVID](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/blob/master/bloc4-5/prog-dynamique/Readme.md) ou une progression en lien avec la programmation dynamique. 

Ces travaux sont à rendre via des dépôts git.  
Il s'agit de cloner les deux dépôts

* [gitlab-fil.univ-lille.fr/diu-eil-lil/icewalker](https://gitlab-fil.univ-lille.fr/diu-eil-lil/icewalker)
* [gitlab-fil.univ-lille.fr/diu-eil-lil/prog-dyn](https://gitlab-fil.univ-lille.fr/diu-eil-lil/prog-dyn)

et de suivre les instructions (inviter les enseignants, commit du
binôme pour s'identifier dans Icewalker, etc.)


Liste des dépôts enregistrés à ce jour
--------------------------------------

_au 27 décembre 2020, 17:25_

### Icewalker ###

Forks de
[gitlab-fil.univ-lille.fr/diu-eil-lil/icewalker](https://gitlab-fil.univ-lille.fr/diu-eil-lil/icewalker)

<!--
https://gitlab-fil.univ-lille.fr/diu-eil-lil/icewalker/-/forks?sort=created_asc
--> 

* Remi Caneri / icewalker
* Patrice Thibaud / icewalker
* Olivier Macquet / icewalker
* Joel Ternoy / icewalker
* Olivier Bracco / icewalker
* Yves Moncheaux / icewalker
* Arnaud Barbier / icewalker
* Bernard Obled / icewalker
* Denis Cresson / icewalker
* Frederic Ducrocq / icewalker
* Gwenael Laurent / icewalker
* Olivier Hennebelle / icewalker
* Stephane Beau / icewalker
* Olivier Seys / icewalker
* David Capitaine / icewalker
* Philippe Poulain / icewalker
* Adrien Willm / icewalker
* Fabrice Delvallee / icewalker
    - et fork de Thierry D'herbomez
	  <!-- https://gitlab-fil.univ-lille.fr/thierry.dherbomez/icewalker -->
* Gilles Coste / icewalker
* Benjamin Lenoir / icewalker
* Sebastien Sauvage / icewalker <!-- formateurs pas membres-->
* Frederic Vicogne / icewalker
    - et fork de Claude Oziard
	  <!-- https://gitlab-fil.univ-lille.fr/claude.oziard/icewalker -->
* Christian Delvarre / icewalker
* Vincent Lesieux / icewalker
* Marie Guichard / icewalker <!-- formateurs pas membres-->
* Thierry Sautiere / icewalker
* Laurent Nowicki / icewalker <!-- formateurs pas membres-->
* Veronique Glacon / icewalker
* Laurent Hennequart / icewalker
* Arnaud Vochot / icewalker
* Christophe Mieszczak / icewalker
* Patrick Rubbens / icewalker
* Thierry Camier / icewalker
* Agnes Engelking / icewalker
* Jean Goujon / icewalker
* Freddy Magniez / icewalker
* Alain Cousin / icewalker
    - et fork de Aldo Florean 
    <!-- https://gitlab-fil.univ-lille.fr/aldo.florean/icewalker -->
* Remi Seynave / icewalker

<!-- hors fork 
https://gitlab-fil.univ-lille.fr/quentin.konieczko/icewalker
https://gitlab-fil.univ-lille.fr/jean.goujon/icewalker
-->

### Projet COVID ou TD prog. dynamique ###

Forks de
[gitlab-fil.univ-lille.fr/diu-eil-lil/prog-dyn](https://gitlab-fil.univ-lille.fr/diu-eil-lil/prog-dyn)

<!--
https://gitlab-fil.univ-lille.fr/diu-eil-lil/prog-dyn/-/forks?sort=created_asc
--> 

* Quentin Konieczko / prog-dyn
* Gwenael Laurent / prog-dyn <!-- formateurs pas membres-->
* Fabrice Delvallee / prog-dyn
* Lionnel Conoir / prog-dyn
* Denis Cresson / prog-dyn
* Pascal Lucas / prog-dyn
* Patrice Thibaud / prog-dyn
* Bernard Obled / prog-dyn
* Vincent Lesieux / prog-dyn
* Thierry Dherbomez / prog-dyn
* Benjamin Lenoir / prog-dyn
* Caroline Estienne / prog-dyn
* Francois Gachelin / prog-dyn
* Stephane Demarthe / prog-dyn
* Philippe Poulain / prog-dyn
* Sebastien Sauvage / prog-dyn
* Frederic Vicogne / prog-dyn
    - et fork de Claude Oziard
	  <!-- https://gitlab-fil.univ-lille.fr/claude.oziard/prog-dyn -->
* Christian Delvarre / prog-dyn
* Christophe Mieszczak / prog-dyn
* Jean Goujon / prog-dyn
* Remi Caneri / prog-dyn
* David Capitaine / prog-dyn
* Marie Guichard / prog-dyn <!-- formateurs pas membres-->
  <!-- aussi 
  https://gitlab-fil.univ-lille.fr/marie.guichard/projet-covid
  -->
* Veronique Glacon / prog-dyn
    - et fork de Agnes Engelking
      <!-- https://gitlab-fil.univ-lille.fr/agnes.engelking/prog-dyn -->
* Thierry Sautiere / prog-dyn <!-- formateurs seulement Guest-->
* Laurent Hennequart / prog-dyn
* Romain Debailleul / prog-dyn
* Yves Moncheaux / prog-dyn
* Patrick Rubbens / prog-dyn
* Stephane Beau / prog-dyn
* Thierry Camier / prog-dyn
* Gilles Coste / prog-dyn
* Olivier Hennebelle / prog-dyn
* Olivier Bracco / prog-dyn  <!-- formateurs pas membres-->
* Arnaud Vochot / prog-dyn
* Freddy Magniez / prog-dyn
  <!-- aussi
  https://gitlab-fil.univ-lille.fr/freddy.magniez/td-programmation-dynamique
  --> 
* Remi Seynave / prog-dyn
* Alain Cousin / prog-dyn  <!-- formateurs pas membres-->
* Arnaud Barbier / prog-dyn

<!-- hors fork
None
-->

<!-- eof --> 
