# Activités débranchées autour des protocoles réseaux

L'objectif de cette partie est de comprendre le fonctionnement des couches protocolaires en simulant le fonctionnement du réseau en mode débranché. Pour cela, nous utiliserons des post-it et des enveloppes. Les post-it représentent les différentes adresses ou ports, les enveloppes représentent les couches protocolaires.

## Couche protocolaire TCP/IP/Ethernet avec liaison via un commutateur

Chaque machine est opéré par plusieurs élèves, un par couche. Nous nous limitons à quatre couches (application, transport, réseau, liaison).
Un élève écrit le message HTTP sur un morceau de papier. L'élève suivant insére le papier dans une enveloppe et colle les deux post-it représentant les numéros de port sur l'enveloppe. Le suivant met l'enveloppe dans une autre représentant IP, puis colle deux post-it pour les adresses IP source et destination. L'élève suivant met l'enveloppe dans une troisième enveloppe qui représente le protocole Ethernet, puis colle deux post-it pour les adresses Ethernet source et destination. Enfin, les élèves qui gèrent le commutateur notent les adresses sources et décident sur quel port (quelle rangée) envoyer les enveloppes. Une duplication des enveloppes est éventuellement nécessaire.


## Routage direct entre trois machines

## Routage de deux réseaux commutés

## Routage de quatre réseaux
